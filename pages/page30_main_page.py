from page_objects import PageObject, PageElement

class MainPage (PageObject):

    Logout_link = PageElement(link_text="Logout")

    def check_page(self):
        return "Logged In" in self.w.title



    def click_logout(self, welcomepage):

        self.Logout_link.click()

        return welcomepage.check_page()


