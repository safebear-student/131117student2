import unittest

import time

from utils import Parameters

from page10_welcome_page import WelcomePage
from page20_login_page import LoginPage
from page30_main_page import MainPage
from page40_frames_page import FramePage

class BaseTest (unittest.TestCase):

    param = Parameters()

    welcomepage = WelcomePage(param.w,param.rootUrl)

    loginpage = LoginPage(param.w,param.rootUrl)

    mainpage = MainPage(param.w, param.rootUrl)

    framespage = FramePage(param.w,param.rootUrl)

    def setUp(self): # to run have a test prefix before the setup

        self.param.w.get(self.param.rootUrl)
        self.param.w.maximize_window()
        assert self.welcomepage.check_page()#

    @classmethod
    def tearDownClass(cls):
        time.sleep(10)
        cls.param.w.quit()