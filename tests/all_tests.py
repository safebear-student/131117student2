import base_test

class TestCases(base_test.BaseTest):

    def test_01_test_login(self):

        # Step 1 - Check we are on the Welcome Page
        assert self.welcomepage.check_page() # Checks true or false

        # Step 2 - Click on Login to check it loads
        assert self.welcomepage.click_login(self.loginpage)

        # Step 3 - Enter username/password and logout.
        assert self.loginpage.login(self.mainpage, "testuser", "testing")

        # Step 4 - Logout
        assert self.mainpage.click_logout(self.welcomepage)